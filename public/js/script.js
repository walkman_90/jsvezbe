'use strict'

let cloneBoxObj = {
	box : document.getElementById("js_box"),
	boxHolder : document.getElementById("js_box_holder"),
	boxCounter : document.getElementById('js_counter'),
	boxCounterValue : 0,
	boxCounterValueStorage : parseInt(localStorage.getItem('counterStorage')),
	boxCloneFn : function(){
		let boxClone = this.box.cloneNode(true);
		this.boxHolder.appendChild(boxClone);
		boxClone.removeAttribute("id");
		boxClone.classList.add("js_box_remove");
	},
	boxCounterFn : function(n){
		this.boxCounterValue += n;
		this.boxCounter.innerHTML = this.boxCounterValue;
	},
	localStorageFn : function(){
		if(this.boxCounterValueStorage > 0) {
			this.boxCounterValue = this.boxCounterValueStorage;
			for(var j = 0; j < this.boxCounterValue; j++) {
				this.boxCloneFn();
			}
		}
		this.boxCounter.innerHTML = this.boxCounterValue;
	},
	cloneFn : function(e){
		if (e.target.id === 'js_box') {
			this.boxCloneFn();
			this.boxCounterFn(1);
			localStorage.setItem('counterStorage', this.boxCounterValue);
		} 
		if (e.target.classList.contains('js_box_remove')) {
			e.target.remove();
			this.boxCounterFn(-1);
			localStorage.setItem('counterStorage', this.boxCounterValue);
		}
	},
	clickEventFn : function(){
		document.querySelector('body').onclick = this.cloneFn.bind(this);
	},
	displayFn : function() {
		this.localStorageFn();
		this.clickEventFn();
	}
}

let counterNameObj = {
	counterName : document.getElementById('js_counter_name'),
	input : document.getElementById('js_input_text'),
	modal : document.getElementById('js_modal'),
	submit : document.getElementById('js_btn_submit'),
    counterNameValueStorage = localStorage.getItem('counterNameStorage');
	getNameFn : function(){
		if(this.input.value.length != 0) {
			let counterNameValue = this.input.value;
			localStorage.setItem('counterNameStorage', counterNameValue);
			localStorage.setItem('counterStorage', cloneBoxObj.boxCounterValue);
			this.counterName.innerHTML = counterNameValue;
			this.modal.style.display = 'none';
		}
	},
	localStorageFn : function(){
		this.counterName.innerHTML = counterNameValueStorage;
		return counterNameValueStorage;
	},
	clickEventFn : function(){
		this.submit.onclick = this.getNameFn.bind(this);
	},
	displayFn: function(){
		this.localStorageFn();
		this.clickEventFn();
	}
}

let mainLocalStorage = {
    storage: [],
    storageElements: {
    	name: counterNameObj.localStorageFn(),
    	value: cloneBoxObj.boxCounterValueStorage
    },
	storageFn : function(){
		this.storage.push(this.storageElements);
		this.storage = this.storage.concat(JSON.parse(localStorage.getItem('names')||'[]'));
		localStorage.setItem('names', JSON.stringify(this.storage));
	},
	displayFn: function(){
		this.storageFn();
	}
}

cloneBoxObj.displayFn();
counterNameObj.displayFn();
mainLocalStorage.displayFn();


// function addNewShow(titleArg, typeArg, genreArg, watchedArg) {
//   var showList = [];

//   var show = {
//     title: titleArg,
//     type: typeArg,
//     genre: genreArg,
//     watched: watchedArg
//   };
//   showList.push(show);
//   showList = showList.concat(JSON.parse(localStorage.getItem('showList')||'[]'));
//   console.log(showList);


//   localStorage.setItem("showList", JSON.stringify(showList));
// };